#!/usr/bin/env python3
#-*- coding: utf-8 -*-

##
# @file gradesalert

##
# @author Quentin Debroise <contact@quentindebroise.com>
#

import sys
import os.path
import subprocess
import urllib.parse
import urllib.request
import xml.etree.ElementTree as ET


# SET YOUR OWN INFORMATION
FREE_MOBILE_USER = "<free-mobile-user-number>"
FREE_MOBILE_SMS_API_KEY = "<free-mobile-sms-api-key>"


def main(argv):
# Go through the new file (each subject)
# for each subject get id and check in current file if exists
# If not then send SMS
    if not os.path.isfile("current_grades.xml"):
        if not os.path.isfile("config"):
            print("No configuration file")
            sys.exit(1)
        subprocess.call("./ensicaen_grades.sh < config >/dev/null", shell=True)
        subprocess.call("mv grades.xml current_grades.xml", shell=True)
        return 0

    subprocess.call("./ensicaen_grades.sh < config >/dev/null", shell=True)
    try:
        fnew = open("grades.xml")
        fold = open("current_grades.xml")
        newgrades = ET.ElementTree(ET.fromstring(fnew.read())).getroot()
        oldgrades = ET.ElementTree(ET.fromstring(fold.read())).getroot()
        fnew.close()
        fold.close()
    except Exception as e:
        print(e)
        sys.exit(1)

    new = []
    newgradesavailable = False
    for subj in newgrades.findall(".//subject"):
        if subj.find("grade") is None: continue
        subjId = subj.attrib["id"]
        newgrade = float(subj[1].text)

        # Check in older file
        oldsubj = oldgrades.find(".//subject[@id='{}']".format(subjId))
        oldgrade = None if oldsubj.find("grade") is None else float(oldsubj[1].text)
        if newgrade != oldgrade:
            new.append((oldsubj[0].text, newgrade))
            newgradesavailable = True
            # Send SMS

    if newgradesavailable:
        textmessage = "Nouvelle(s) note(s) disponible(s)"
        for label, grade in new:
            textmessage += "\n---\n{}\n{}".format(label, grade)
        url = "https://smsapi.free-mobile.fr/sendmsg?user={}&pass={}&msg={}".format(
                    FREE_MOBILE_USER,
                    FREE_MOBILE_SMS_API_KEY,
                    urllib.parse.quote_plus(textmessage))
        urllib.request.urlopen(url)

if(__name__ == '__main__'):
    sys.exit(main(sys.argv))



