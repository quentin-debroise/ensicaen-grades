<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" version="1.0" encoding="iso-8859-1" indent="yes" />
    <!--
    Number of characters without the '@'
    xmllint -\-html -\-noout dump.html -\-xpath "string-length(translate(//tbody/tr[5]/td[3]/text(), '@', ''))" 32
    Remainder of string after 5 '@' as been removed
    xmllint -\-html -\-noout dump.html -\-xpath "substring-after(//tbody/tr[5]/td[3]/text(), '@@@@@')"
    Length of that string
    xmllint -\-html -\-noout dump.html -\-xpath "string-length(substring-after(//tbody/tr[5]/td[3]/text(), '@@@@@'))"  41
    => 41 != 32 so not of interest

    For all tr ue -> apply template for:
        - all tr whose first previous-sibling is self(ue)

    //tr[string-length(td[3]/text()) > 0]   All tr whose td[3] length is > 0
    -->

    <xsl:template match="/">
        <grades>
            <!--All 'tr' which length of their 3rd 'td' (without @) is same as length of 3rd 'td' substring after matching 10 @ (+1 for the extra space at the beggining)-->
            <!--<xsl:apply-templates select="//tr[string-length(translate(td[3]/text(), '@', '')) = string-length(substring-after(td[3]/text(), '@@@@@@@@@@')) + 1]" mode="ue" />-->
        <!--Semesters doesn't have an extra space and we have to make sure that they contains a '@'-->
        <xsl:apply-templates select="//tr[contains(td[3]/text(), '@') and string-length(translate(td[3]/text(), '@', '')) = string-length(substring-after(td[3]/text(), '@@@@@'))]" mode="semestre" />
        </grades>
    </xsl:template>

    <xsl:template match="tr" mode="semestre">
        <semestre>
            <xsl:attribute name="id">
                <xsl:value-of select="position()"/>
            </xsl:attribute>
            <xsl:apply-templates select="following-sibling::*[preceding-sibling::tr[contains(td[3]/text(), '@') and string-length(translate(td[3]/text(), '@', '')) = string-length(substring-after(td[3]/text(), '@@@@@'))][1] = current() and string-length(translate(td[3]/text(), '@', '')) = string-length(substring-after(td[3]/text(), '@@@@@@@@@@')) + 1]" mode="ue" />
        </semestre>
    </xsl:template>

    <xsl:template match="tr" mode="ue">
        <ue>
            <xsl:attribute name="id">
                <xsl:value-of select="normalize-space(td[2]/text())" />
            </xsl:attribute>
            <name><xsl:value-of select="normalize-space(td[3]/text())"/></name>
            <!--Match all following sibling whose first previous sibling matching the expression above is the same as the current one. Among those we take the ones that are modules-->
            <xsl:apply-templates select="following-sibling::*[preceding-sibling::tr[string-length(translate(td[3]/text(), '@', '')) = string-length(substring-after(td[3]/text(), '@@@@@@@@@@')) + 1][1] = current() and string-length(translate(td[3]/text(), '@', '')) = string-length(substring-after(td[3]/text(), '@@@@@@@@@@@@@@@')) + 1]" mode="module"/>
        </ue>
    </xsl:template>

    <xsl:template match="tr" mode="module">
        <module>
            <xsl:attribute name="id">
                <xsl:value-of select="normalize-space(td[2]/text())" />
            </xsl:attribute>
            <name><xsl:value-of select="normalize-space(td[3]/text())"/></name>
            <!--Same process as above but for modules/subjects-->
            <xsl:apply-templates select="following-sibling::*[preceding-sibling::tr[string-length(translate(td[3]/text(), '@', '')) = string-length(substring-after(td[3]/text(), '@@@@@@@@@@@@@@@')) + 1][1] = current() and string-length(translate(td[3]/text(), '@', '')) = string-length(substring-after(td[3]/text(), '@@@@@@@@@@@@@@@@@@@@')) + 1]" mode="subject" />
        </module>
    </xsl:template>

    <xsl:template match="tr" mode="subject">
        <subject>
            <xsl:apply-templates select="td[position() > 1 and not(position() > 4)]" />
        </subject>
    </xsl:template>

    <xsl:template match="td">
        <xsl:variable name="value" select="normalize-space(current()/text())"/>

        <xsl:if test="current()/text() != ''">
            <xsl:choose>
                <xsl:when test="position() = 1">
                    <xsl:attribute name="id">
                        <xsl:value-of select="$value"/>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="position() = 2">
                    <name><xsl:value-of select="$value"/></name>
                </xsl:when>
                <xsl:when test="position() = 3">
                    <grade><xsl:value-of select="$value"/></grade>
                </xsl:when>
            </xsl:choose>
        </xsl:if>
    </xsl:template>

</xsl:stylesheet>
