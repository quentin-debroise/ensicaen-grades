<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" version="1.0" encoding="iso-8859-1" indent="yes" />

    <xsl:variable name="lowercase" select="abcdefghijklmnopqrstuvwxyz" />
    <xsl:variable name="uppercase" select="ABCDEFGHIJKLMNOPQRSTUVWXYZ" />

    <xsl:template match="/">
        <coefs>
            <xsl:apply-templates select="//tr">
            </xsl:apply-templates>
        </coefs>
    </xsl:template>

    <xsl:template match="tr">
        <!--<xsl:if test="td[contains(@class, 'couleurMajeure')]">-->
            <!--<ue>-->
                <!--<xsl:attribute name="id"><xsl:value-of select="td[contains(@class, 'couleurMajeure')]/b[1]"/></xsl:attribute>-->
                <!--<xsl:value-of select="td[contains(@class, 'couleurMajeure')]/b[2]"/>-->
            <!--</ue>-->
        <!--</xsl:if>-->

        <subject>
            <xsl:choose>
                <xsl:when test="count(td) = 10">
                    <xsl:attribute name="id">
                        <xsl:value-of select="translate(td[3], 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
                    </xsl:attribute>
                    <xsl:if test="td[7]/text() != '-'">
                        <xsl:attribute name="partiel">
                            <xsl:value-of select="number(td[7][text() != '-'])" />
                        </xsl:attribute>
                    </xsl:if>
                    <xsl:if test="td[8]/text() != '-'">
                        <xsl:attribute name="exam">
                            <xsl:value-of select="number(td[8][text() != '-'])" />
                        </xsl:attribute>
                    </xsl:if>
                    <xsl:if test="td[9]/text() != '-'">
                        <xsl:attribute name="tp">
                            <xsl:value-of select="number(td[9][text() != '-'])" />
                        </xsl:attribute>
                    </xsl:if>
                    <!--<coef-partiel><xsl:value-of select="number(td[7])"/></coef-partiel>-->
                    <!--<coef-exam><xsl:value-of select="number(td[8])"/></coef-exam>-->
                    <!--<coef-tp><xsl:value-of select="number(td[9])"/></coef-tp>-->
                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name="id">
                        <xsl:value-of select="translate(td[2], 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
                    </xsl:attribute>
                    <xsl:if test="td[6]/text() != '-'">
                        <xsl:attribute name="partiel">
                            <xsl:value-of select="number(td[6][text() != '-'])" />
                        </xsl:attribute>
                    </xsl:if>
                    <xsl:if test="td[7]/text() != '-'">
                        <xsl:attribute name="exam">
                            <xsl:value-of select="number(td[7][text() != '-'])" />
                        </xsl:attribute>
                    </xsl:if>
                    <xsl:if test="td[8]/text() != '-'">
                        <xsl:attribute name="tp">
                            <xsl:value-of select="number(td[8][text() != '-'])" />
                        </xsl:attribute>
                    </xsl:if>
                    <!--<coef-partiel><xsl:value-of select="number(td[6])"/></coef-partiel>-->
                    <!--<coef-exam><xsl:value-of select="number(td[7])"/></coef-exam>-->
                    <!--<coef-tp><xsl:value-of select="number(td[8])"/></coef-tp>-->
                </xsl:otherwise>
            </xsl:choose>
        </subject>
    </xsl:template>

</xsl:stylesheet>
