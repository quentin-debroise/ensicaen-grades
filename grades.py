#!/usr/bin/env python3
#-*- coding: utf-8 -*-
#
# Author: Quentin Debroise <debroise@ecole.ensicaen.fr>
# Last update: 27th of May 2018
#
#

import sys
import argparse
import xml.etree.ElementTree as ET

class BashColors:
    RESET = "\033[0;0m"
    BLACK = "\033[0;30m"
    RED = "\033[0;31m"
    GREEN = "\033[0;32m"
    YELLOW = "\033[0;33m"
    BLUE = "\033[1;34m"
    PINK = "\033[0;35m"
    CYAN = "\033[0;36m"
    BOLD_YELLOW = "\033[1;33m"
    BOLD_GREEN = "\033[1;32m"
    BG_CYAN_FG_BLACK = "\033[1;46;30m"

def parse_args():
    parser = argparse.ArgumentParser(description="Calculates grades", epilog="Example:\n./grades.py --see all -o out.html && firefox out.html")
    parser.add_argument("grades", type=str, nargs="?", default="grades.xml", help="XML grades file (defaults to grades.xml)")
    parser.add_argument("coefs", type=str, nargs="?", default="coefs.xml", help="XML coefficents file (defaults to coefs.xml)")
    parser.add_argument("-o", type=str, nargs="?", help="Output results to a file")
    parser.add_argument("-m", type=str, nargs="?", choices=["html", "csv"], default="html", help="Select output method")
    parser.add_argument("--see", type=str, nargs="?", choices=["all", "semester", "ue", "module"], help="Select what to see (defaults to 'all')")
    parser.add_argument("--amisad", action="store_true", help="Check UE validation")
    parser.add_argument("--details", action="store_true", help="Display the details of the calculations")
    parser.add_argument("--predict", action="store_true", help="Allows entering prediction for missing grades")
    args = parser.parse_args()
    return args


class HTMLOutput:
    def __init__(self, xmlgrades, xmlcoefs):
        self.xmlgrades = xmlgrades
        self.xmlcoefs = xmlcoefs
        self.doc = ET.parse("html/template.html")
        self.warnings = []

    def insert_empty_td(self, tr, n):
        for i in range(n):
            ET.SubElement(tr, "td")

    def _create_headers(self, table):
        tr = ET.SubElement(table, "tr")
        for header in ("Semestre", "UE", "Module", "Matière", "Note", "Coef"):
            ET.SubElement(tr, "th").text = header

    def _create_table_semesters(self, table, xmlgrades):
        for i, xmlsemester in enumerate(xmlgrades):
            tr = ET.SubElement(table, "tr", attrib={"class": "semester"})
            ET.SubElement(tr, "td").text = "Semestre %s" % str(i + 1)
            self.insert_empty_td(tr, 3)
            res = calculate_semester(xmlsemester, self.xmlcoefs)
            ET.SubElement(tr, "td").text = str(avg(res))
            ET.SubElement(tr, "td").text = str(res[1])
            self._create_table_ues(table, xmlsemester)

    def _create_table_ues(self, table, xmlsemester):
        for xmlue in xmlsemester:
            tr = ET.SubElement(table, "tr", attrib={"class": "ue"})
            self.insert_empty_td(tr, 1)
            ET.SubElement(tr, "td").text = xmlue[0].text

            self.insert_empty_td(tr, 2)
            res = calculate_ue(xmlue, self.xmlcoefs)
            ET.SubElement(tr, "td").text = str(avg(res))
            ET.SubElement(tr, "td").text = str(res[1])
            self._create_table_modules(table, xmlue)

    def _create_table_modules(self, table, xmlue):
        for xmlmodule in xmlue.findall("module[subject]"):
            moduleID = xmlmodule.attrib["id"]
            tr = ET.SubElement(table, "tr", attrib={"class": "module"})
            self.insert_empty_td(tr, 2)
            ET.SubElement(tr, "td").text = xmlmodule[0].text

            self.insert_empty_td(tr, 1)
            res = calculate_module(xmlmodule, self.xmlcoefs)
            ET.SubElement(tr, "td").text = str(avg(res))
            ET.SubElement(tr, "td").text = str(res[1])
            self._create_table_subjects(table, xmlmodule, moduleID)

    def _create_table_subjects(self, table, xmlmodule, moduleID):
        for xmlsubject in xmlmodule.findall("subject"):
            moduleID = fix_coef_incoherence(moduleID, xmlsubject)
            coef = get_coef(self.xmlcoefs, xmlsubject, moduleID)
            if xmlsubject.find("grade") is None:
                grade = "N/A"
                self.warnings.append("Aucune note disponible pour '%s'" % (xmlsubject[0].text))
            else:
                grade = xmlsubject[1].text
            # grade = "N/A" if xmlsubject.find('grade') is None else xmlsubject[1].text

            tr = ET.SubElement(table, "tr", attrib={"class": "subject"})
            self.insert_empty_td(tr, 3)
            ET.SubElement(tr, "td").text = xmlsubject[0].text
            ET.SubElement(tr, "td").text = grade
            ET.SubElement(tr, "td").text = str(coef)

    def _create_total(self, table):
        tr = ET.SubElement(table, "tr")
        ET.SubElement(tr, "td").text = "Moyenne générale"
        self.insert_empty_td(tr, 3)
        res = calculate_year(self.xmlgrades, self.xmlcoefs)
        ET.SubElement(tr, "td").text = str(avg(res))
        ET.SubElement(tr, "td").text = str(res[1])

    def _create_warnings(self):
        section = ET.SubElement(self.doc.find(".//body"), "section", attrib={"id": "warnings"})
        ET.SubElement(section, "h2").text = "Attention!"
        for warn in self.warnings:
            ET.SubElement(section, "p").text = warn

    def create_grades_table(self):
        # ET.ElementTree.SubElement(parent, tag, attrib={})
        section = ET.SubElement(self.doc.find(".//body"), "section", attrib={"id": "grades-table"})
        ET.SubElement(section, "h1").text = "Notes"
        table = ET.SubElement(section, "table")

        self._create_headers(table)
        self._create_table_semesters(table, self.xmlgrades)
        self._create_total(table)
        self._create_warnings()

    def create_ue_table(self):
        section = ET.SubElement(self.doc.find(".//body"), "section", attrib={"id": "ue-table"})
        ET.SubElement(section, "h1").text = "UE"
        table = ET.SubElement(section, "table")
        for i, xmlsemester in enumerate(self.xmlgrades):
            # self.insert_row("Semestre %s" % str(i + 1))
            tr = ET.SubElement(table, "tr")
            ET.SubElement(tr, "td").text = "Semestre %s" % str(i + 1)
            for xmlue in xmlsemester.findall(".//ue"):
                avgUE = avg(calculate_ue(xmlue, self.xmlcoefs))
                res = "RATTRAPAGE" if avgUE < 10 else "VALIDE"
                classAtt = "retake" if avgUE < 10 else "pass"
                # self.insert_row("", xmlue[0].text, str(avgUE), res)
                tr = ET.SubElement(table, "tr", attrib={"class": classAtt})
                self.insert_empty_td(tr, 1)
                ET.SubElement(tr, "td").text = xmlue[0].text
                ET.SubElement(tr, "td").text = str(avgUE)
                ET.SubElement(tr, "td").text = res

    def generate(self, filename):
        with open(filename, "w") as f:
            self.doc.write(filename)


class CSVOutput:
    def __init__(self, xmlgrades, xmlcoefs, fs=";"):
        self.xmlgrades = xmlgrades
        self.xmlcoefs = xmlcoefs
        self.csv = []
        self.fs = fs

    def create_grades_table(self):
        self.insert_row("Semestre", "UE", "Module", "Matière", "Coefficient", "Note")
        for i, xmlsemester in enumerate(self.xmlgrades):
            self.insert_row("Semestre %s" % str(i + 1))
            for xmlue in xmlsemester:
                self.insert_row("", xmlue[0].text)
                for xmlmodule in xmlue.findall("module[subject]"):
                    moduleID = xmlmodule.attrib["id"]
                    self.insert_row("", "", xmlmodule[0].text)
                    for xmlsubject in xmlmodule.findall("subject"):
                        moduleID = fix_coef_incoherence(moduleID, xmlsubject)
                        coef = get_coef(self.xmlcoefs, xmlsubject, moduleID)
                        grade = "" if xmlsubject.find('grade') is None else xmlsubject[1].text
                        self.insert_row("", "", "", xmlsubject[0].text, str(coef), grade)

    def create_ue_table(self):
        for i, xmlsemester in enumerate(self.xmlgrades):
            self.insert_row("Semestre %s" % str(i + 1))
            for xmlue in xmlsemester.findall(".//ue"):
                avgUE = avg(calculate_ue(xmlue, self.xmlcoefs))
                res = "RATTRAPAGE" if avgUE < 10 else "VALIDE"
                self.insert_row("", xmlue[0].text, str(avgUE), res)

    def insert_row(self, *args):
        self.csv.append(args)

    def insert_empty_row(self):
        self.insert_row()

    def generate(self, filename):
        for i, row in enumerate(self.csv):
            self.csv[i] = self.fs.join(row)
        with open(filename, "w") as out:
            out.write("\n".join(self.csv))



def avg(total):
    if total[1] == 0: return 0
    return round(total[0] / total[1], 3)



# Fixes incoherence in Modules IDs because the format is so messed up. pfff...
def fix_coef_incoherence(moduleID, xmlsubject):
    # LV2 are xxxxx1/2/3 depending on the LV2 but only xxxxx0 figures in the coefs list (1nd year)
    if "1L1AA21" in moduleID or "1L2AA21" in moduleID:
        return moduleID[:-1] + '0'
    # LV2 are xxxxx1/2/3 depending on the LV2 but only xxxxx1 figures in the coefs list (2nd year)
    if "espagnol" in xmlsubject[0].text.lower():
        return moduleID[:-1] + '1'
    # Scala subject ID is completely fucked up, 2I3B1 against 2I1AE1 in the coefs. Man!...
    # if "scala" in xmlsubject[0].text.lower()):
    if "2I3B1" == moduleID:
        return "2I1AE1"
    return moduleID



def enter_prediction():
    pred = input("Enter grade: ")
    print(BashColors.RESET)
    try:
        pred = float(pred)
    except ValueError:
        print("%sError%s" % (BashColors.RED, BashColors.RESET))
        return 0
    if pred < 0 or pred > 20:
        print("%sError%s" % (BashColors.RED, BashColors.RESET))
        return 0
    return pred

def get_coef(xmlcoefs, xmlsubject, moduleID):
    subjectType = xmlsubject.attrib["id"][0]
    coef = 1
    try:
        if subjectType == 'P':
            coef = float(xmlcoefs.find(".//subject[@id='{}']".format(moduleID)).attrib["partiel"])
        elif subjectType == 'E':
            coef = float(xmlcoefs.find(".//subject[@id='{}']".format(moduleID)).attrib["exam"])
        elif subjectType == 'T':
            coef = float(xmlcoefs.find(".//subject[@id='{}']".format(moduleID)).attrib["tp"])
    except AttributeError:
        print("{}\033[1;31m[ERROR] Problem in coefficient ID ({}) on '{}'"
              "please report this bug <debroise@ecole.ensicaen.fr>{}".format(
                  BashColors.RED, moduleID, xmlsubject[0].text, BashColors.RESET))
        sys.exit(1)
    return coef

def calculate_subject(xmlsubject, xmlcoefs, moduleID, prediction=False, display=False):
    moduleID = fix_coef_incoherence(moduleID, xmlsubject)
    coef = get_coef(xmlcoefs, xmlsubject, moduleID)
    grade = None if xmlsubject.find('grade') is None else float(xmlsubject[1].text)

    predicted = (0, 0)
    predictedGrade = 0
    if grade is None:
        if prediction:
            print("{}MISSING GRADE FOR '{}' COEF: {}".format(BashColors.BG_CYAN_FG_BLACK, xmlsubject[0].text, coef))
            predictedGrade = enter_prediction()
            predicted = (predictedGrade * coef, coef)
        else:
            print("{}/!\\ WARNING: SKIPPING MISSING GRADES FOR '{}' /!\\{}".format(BashColors.BOLD_YELLOW, xmlsubject[0].text, BashColors.RESET))

    if display:
        gradeDisp = predictedGrade if grade is None else float(xmlsubject[1].text)
        color = BashColors.BOLD_GREEN if gradeDisp > 15 else BashColors.GREEN if gradeDisp > 10 else BashColors.YELLOW if gradeDisp > 5 else BashColors.RED
        print("\t\t\t{}\t{}[{}]{} ({})".format(xmlsubject[0].text, color, gradeDisp, BashColors.RESET, coef))

    if grade is None:
        return predicted
    return (grade * coef, coef)

def calculate_module(xmlmodule, xmlcoefs, prediction=False, display=False):
    if display:
        print("\t\t{}{}{}".format(BashColors.CYAN, xmlmodule[0].text, BashColors.RESET))

    moduleID = xmlmodule.attrib["id"]
    total = [0, 0]
    for xmlsubject in xmlmodule.findall("subject"):
        total = list(map(sum, zip(total, calculate_subject(xmlsubject, xmlcoefs, moduleID, display=display, prediction=prediction))))
    return total

def calculate_ue(xmlue, xmlcoefs, prediction=False, display=False):
    if display:
        print("\n\t{}{}{}".format(BashColors.PINK, xmlue[0].text, BashColors.RESET))

    total = [0, 0]
    for xmlmodule in xmlue.findall("module[subject]"):
        total = list(map(sum, zip(total, calculate_module(xmlmodule, xmlcoefs, display=display, prediction=prediction))))
    return total

def calculate_semester(xmlsemester, xmlcoefs, prediction=False, display=False):
    total = [0, 0]
    for xmlue in xmlsemester:
        total = list(map(sum, zip(total, calculate_ue(xmlue, xmlcoefs, display=display, prediction=prediction))))
    return total

def calculate_year(xmlyear, xmlcoefs, prediction=False, display=False):
    total = [0, 0]
    for i, xmlsemester in enumerate(xmlyear):
        if display:
            print("{}Semestre {}{}".format(BashColors.BLUE, str(i + 1), BashColors.RESET))
        total = list(map(sum, zip(total, calculate_semester(xmlsemester, xmlcoefs, display=display, prediction=prediction))))
    return total



def display_choices(choices):
    print("")
    for i, value in enumerate(choices):
        print("  {}) {}".format(i + 1, value[1]))

def get_semester_list(xmlgrades):
    nbSemesters = len(xmlgrades.findall(".//semestre"))
    return tuple((("", "Semestre " + str(i + 1)) for i in range(nbSemesters)))

def get_ue_list(xmlgrades, semester):
    xpath = ".//semestre[@id='{}']/ue".format(semester)
    return tuple((xmlue.attrib["id"], xmlue[0].text) for xmlue in xmlgrades.findall(xpath))

def get_module_list(xmlgrades, ue):
    xpath = ".//ue[@id='{}']/module".format(ue)
    return tuple((xmlmodule.attrib["id"], xmlmodule[0].text) for xmlmodule in xmlgrades.findall(xpath))

def get_subject_list(xmlgrades, module):
    xpath = ".//module[@id='{}']/subject".format(module)
    return tuple((( xmlsubject.attrib["id"], xmlsubject[0].text) for xmlsubject in xmlgrades.findall(xpath)))



def check_int_input(choice, interval):
    try:
        choice = int(choice)
    except ValueError:
        return False
    if choice - 1 < interval[0] or choice - 1 >= interval[1]:
        return False
    return True

# TODO: review this selecting stuff
def select_semester(xmlgrades):
    semList = get_semester_list(xmlgrades)
    display_choices(semList)

    choice = input("Quel semestre: ")
    if (not check_int_input(choice, [0, len(semList)])):
        print("{}Error in input{}".format(BashColors.RED, BashColors.RESET))
        sys.exit(1)
    return int(choice)

def select_ue(xmlgrades, semester):
    ueList = get_ue_list(xmlgrades, semester)
    display_choices(ueList)

    choice = input("Quelle UE: ")
    if (not check_int_input(choice, [0, len(ueList)])):
        print("{}Error in input{}".format(BashColors.RED, BashColors.RESET))
        sys.exit(1)
    return ueList[int(choice) - 1][0]

def select_module(xmlgrades, ue):
    moduleList = get_module_list(xmlgrades, ue)
    display_choices(moduleList)

    choice = input("Which module: ")
    if (not check_int_input(choice, [0, len(moduleList)])):
        print("{}Error in input{}".format(BashColors.RED, BashColors.RESET))
        sys.exit(1)
    return moduleList[int(choice) - 1][0]



def get_semester(xmlgrade, semester):
    return xmlgrade.find(".//semestre[@id='{}']".format(semester))

def get_ue(xmlgrade, ue):
    return xmlgrade.find(".//ue[@id='{}']".format(ue))

def get_module(xmlgrades, module):
    return xmlgrades.find(".//module[@id='{}']".format(module))



def get_semester_grade(xmlgrades, xmlcoefs, display=False, prediction=False):
    return avg(calculate_semester(
        get_semester(xmlgrades, select_semester(xmlgrades)),
        xmlcoefs, display=display, prediction=prediction))
def get_ue_grade(xmlgrades, xmlcoefs, display=False, prediction=False):
    return avg(calculate_ue(
        get_ue(xmlgrades, select_ue(
            xmlgrades, select_semester(xmlgrades))),
        xmlcoefs, display=display, prediction=prediction))
def get_module_grade(xmlgrades, xmlcoefs, display=False, prediction=False):
    return avg(calculate_module(
        get_module(xmlgrades, select_module(
            xmlgrades, select_ue(
                xmlgrades, select_semester(xmlgrades)))),
        xmlcoefs, display=display, prediction=prediction))



def check_ue_passed(xmlgrades, xmlcoefs, semester, prediction=False):
    xpath = ".//semestre[@id='{}']/ue".format(semester)
    for xmlue in xmlgrades.findall(xpath):
        print("\n" + xmlue[0].text)
        if avg(calculate_ue(xmlue, xmlcoefs, prediction=prediction)) < 10.0:
            print("{}\tRATTRAPAGE{}".format(BashColors.RED, BashColors.RESET))
        else:
            print("{}\tVALIDE{}".format(BashColors.GREEN, BashColors.RESET))


# TODO
# - Compute notes to have so that UE passes
def main(argv):
    if len(sys.argv) < 1:
        print("Use ./grades.py -h for usage")
        sys.exit(0)
    args = parse_args()

    try:
        fgrades = open(args.grades, "r")
        fcoefs = open(args.coefs, "r")
    except FileNotFoundError as e:
        print(e)
        sys.exit(1)
    xmlgrades = ET.ElementTree(ET.fromstring(fgrades.read())).getroot()
    xmlcoefs = ET.ElementTree(ET.fromstring(fcoefs.read())).getroot()
    # xmlgrades = ET.parse(args.grades).getroot()
    # xmlcoefs = ET.parse(args.coefs).getroot()
    fgrades.close()
    fcoefs.close()

    if args.see is not None:
        if args.see == "all":
            print("Moyenne de l'année:", avg(calculate_year(xmlgrades, xmlcoefs, display=args.details, prediction=args.predict)))
        elif args.see == "semester":
            print("Moyenne du semestre", get_semester_grade(xmlgrades, xmlcoefs, display=args.details, prediction=args.predict))
        elif args.see == "ue":
            print("Moyenne de l'UE", get_ue_grade(xmlgrades, xmlcoefs, display=args.details, prediction=args.predict))
        elif args.see == "module":
            print("Moyenne du module", get_module_grade(xmlgrades, xmlcoefs, display=args.details, prediction=args.predict))

    if args.amisad:
        print("\n\n\033[0;30;43m===== Am I Sad ? =====\033[0;0m")
        check_ue_passed(xmlgrades, xmlcoefs, select_semester(xmlgrades), prediction=args.predict)

    if args.o is not None:
        filename = args.o.split('.')[0] + "." + args.m
        if args.m == "html":
            html = HTMLOutput(xmlgrades, xmlcoefs)
            html.create_grades_table()
            html.create_ue_table()
            html.generate(filename)
        else:
            csv = CSVOutput(xmlgrades, xmlcoefs)
            csv.create_grades_table()
            csv.insert_empty_row()
            csv.create_ue_table()
            csv.generate(filename)

if __name__ == '__main__':
    sys.exit(main(sys.argv))



