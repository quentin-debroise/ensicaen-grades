# Ensicaen Grades

## This is no longer maintained

Fetches grades from Ensicaen ENT and processes them. 

/!\ It only works for computer science students.

## Author
Quentin Debroise <debroise@ecole.ensicaen.fr>

## Dependencies
- cURL (tested with 7.35.0)
- xmllint 
- xsltproc
- tidy (w3c tool)
- python3 (tested with 3.5)

## Build & Run
First install all the dependencies
```
apt-get install xsltproc libxml2-utils curl tidy
```
then clone and run the script
```
git clone https://framagit.org/Wafflesys/ensicaen-grades.git
cd ensicaen_grades/
chmod u+x ensicaen_grades.sh grades.py

./ensicaen_grades.sh # Fetch the grades
./grades.py --details --see all # Processe the grades
```
It will ask you for your credentials and which grades you want.

## Documentation
You can see all the possibilities of the python script by using `./grades -h`

- `--details` will show you all your grades depending on what you want to see semester, UE, module, ...
- `--see TYPE` type of grades you want to see (TYPE is among 'all', 'semester', 'ue', 'module')
- `--amisad` to see which EU you will have to retake
- `--predict` to enter prediction for grades not available
- `-o FILE` to output results in a file (CSV or HTML available)
- `-m METHOD` Choose output method (among 'csv', 'html')

```
Examples
./grades --amisad --predict # Check UE and allow you to enter value for missing grades
./grades --see ue # Computes the grade for an UE
./grades --details --see ue # Computes the grade for an UE showing the grades detail
./grades -o grades.html # Outputs in html file
```

##### Bonus :D
`./ensicaen_grades.sh && ./grades.py -o grades.html && firefox grades.html`


## Raspberry PI setup
You can setup a raspberry pi or anything else to run the script periodically. The script `gradesalert.py` is the one to run. It will fetch grades and compare with the one it already has. If new ones are detected then it will send an SMS using the Free Mobile SMS API. (If you don't have a Free SIM card you can change the script so that it sends you an email, or any other way to inform you).

Firt open the file `config` and replace the fields accordingly.
Then open up `gradesalert.py` and put your free mobile user number (your login) and your sms api key. (To obtain this go in your userspace and in your options you can activate 'sms notifications') at the beginning of the file.

You can initialize the script by running it once manually if you want to so that it fetches the existing grades.
Then just setup a cron task to running when you want.

## Bugs
Please report any bug you encounter. Especially when you encounter something like `[ERROR] Problem in coefficient ID (2L1AA22) on 'Exam LV2 - Espagnol' please report this bug <debroise@ecole.ensicaen.fr>`
#####(Ce problème peut survenir parce que pour certaines matières il n'y a pas le même identifiant de la matière affiché sur le dossier de notes et l'id affiché sur la plateforme pédagogique. Vive l'ENT o/)

## License
- Feel free to reuse this work, copy, modify and share. Giving attribution is always appreciated though :)
- Feel free to modify the python script to adapt it to your own needs. (And why not make pull requests!)
- Reuse the XML files for any other purposes since they are quite nice (and were tough to extract!) 

- The CAS and Shibboleth authentication can be interesting for other purposes (Largely inspired and adapted from https://gist.github.com/olberger/d34253822a84664648b3)
