#!/bin/bash
#
# Author: Quentin Debroise <debroise@ecole.ensicaen.fr>
# Last update: 26th of May 2018
#
# Description: Bash script to access Ensicaen ENT and gather the grades for further processing
#
# Usage: ./ensicaen_grades.sh
#
# Dependencies:
#   - curl (Tested with curl 7.35.0)
#   - tidy
#   - xsltproc (Tested with libxml version 20901)
#   - xmllint (Tested with libxml version 20901)
# 

cd "${BASH_SOURCE%/*}" || exit 

URL_WAIF="https://wayf.normandie-univ.fr/WAYF.php?entityID=https%3A%2F%2Fent.normandie-univ.fr&return=https%3A%2F%2Fent.normandie-univ.fr%2FShibboleth.sso%2FWAYF%3FSAMLDS%3D1%26target%3Dcookie%253A433a532c"
URL_CAS="https://cas.ensicaen.fr/cas/login?service=https%3A%2F%2Fshibboleth.ensicaen.fr%2Fidp%2FAuthn%2FRemoteUser"

IDP="https%3A%2F%2Fshibboleth.ensicaen.fr%2Fidp%2Fshibboleth"

USER_AGENT="Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0"
COOKIE_JAR=".cookie-jar"
HEADER_DUMP=".headers"
SAML_RESPONSE=".samlresponse"

HTML_DUMP="dump.html"
HTML_CLEANED="clean.html"

OUTPUT_GRADES="grades.xml"
OUTPUT_COEFS="coefs.xml"

XSL_GRADES="xsl/grades.xsl"
XSL_COEFS="xsl/coefs.xsl"

#
# Connects to Ensicaen CAS
#
login_cas(){
    echo "Connecting to CAS Ensicaen..."
    # Access ent.normandie-univ.fr to get the required cookie _idp_authn_lc_key for next steps
    curl -L -s -c $COOKIE_JAR --data "user_idp=$IDP" "https://ent.normandie-univ.fr" -o /dev/null
    # Access Ensicaen CAS login page from ent.normandie-univ.fr and get the hidden unique CAS_ID
    CAS_ID=$(curl -s -L --data "user_idp=$IDP" -c "$COOKIE_JAR" "$URL_WAIF" | grep "name=.lt" | cut -d'"' -f6)
    # Read username:password
    read -p "Ensicaen CAS Username: " USERNAME
    read -sp "Ensicaen CAS Password: " PASSWORD
    # Submit the login form
    payload="username=$USERNAME&password=$PASSWORD&lt=$CAS_ID&execution=e1s1&_eventId=submit&submit=LOGIN"
    curl -s --data "$payload" -i -b "$COOKIE_JAR" -c "$COOKIE_JAR" "$URL_CAS" -D"$HEADER_DUMP" -o /dev/null
    # Get the ticket and check access
    TICKET=$(grep Location "$HEADER_DUMP" | sed 's/Location: //' | sed 's///') # Dos2Unix transformation or it might cause problems later
    if [[ "$TICKET" == "" ]]; then
        echo -e "\n\e[0;31mError: username or password incorrect! \e[0;0m"
        exit 1
    fi
}

#
# Authenticate against Shibboleth
#
login_shibboleth(){
    echo -e "\n\nAuthentication with Shibboleth....";
    curl -s -L -c "$COOKIE_JAR" -b "$COOKIE_JAR" "$TICKET" -D"$HEADER_DUMP" -o "$HTML_DUMP";
    # Get the target url, relayState and SAML response
    target=$(cat "$HTML_DUMP" | grep "form action" | cut -d'"' -f2);
    relayState=$(cat "$HTML_DUMP" | grep "RelayState" | cut -d'"' -f6 | sed 's/:/%3A/');
    $(cat "$HTML_DUMP" | grep "SAMLResponse" | cut -d'"' -f6 > "$SAML_RESPONSE");
    # Authenticate using the previous data
    curl -L -s --data "RelayState=$relayState" --data-urlencode SAMLResponse@"$SAML_RESPONSE" -b $COOKIE_JAR -c $COOKIE_JAR "$target" -o "$HTML_DUMP";
    echo -e "\e[0;32mConnected to the ENT! \e[0;0m\n"
}

# The obtained file mixes all the different grades. grades inside a module and the module final grade for instance.
fetch_grades(){
    echo "Accessing grades page...";
    # Accessing "Mon dossier web"
    dossierURL=$(cat "$HTML_DUMP" | grep -o "/uPortal/f/[a-z0-9]\+/p/esup-mondossierweb2.[a-z0-9]\+");
    curl -L -s -b "$COOKIE_JAR" -c "$COOKIE_JAR" "https://ent.normandie-univ.fr$dossierURL/max/render.uP?pCp" -o "$HTML_DUMP"

    # Accessing "Notes et resultats"
    viewstate=$(cat "$HTML_DUMP" | grep "javax.faces.ViewState" | cut -d'"' -f14 | head -n1 | sed 's/=/%3D/g')
    curl -L -s -b "$COOKIE_JAR" -c "$COOKIE_JAR" --data "formMenu_SUBMIT=1&formMenu%3A_idcl=formMenu%3Alinknotes1&formMenu%3A_link_hidden_=&javax.faces.ViewState=$viewstate" "https://ent.normandie-univ.fr$dossierURL/max/action.uP?pP_org.apache.myfaces.portlet.MyFacesGenericPortlet.VIEW_ID=%2Fstylesheets%2Fetu%2Fwelcome.xhtml" -o "$HTML_DUMP"

    select_grades;

    # Accessing the requested year
    viewstate=$(cat "$HTML_DUMP" | grep "javax.faces.ViewState" | cut -d'"' -f14 | head -n1 | sed 's/=/%3D/g');
    payload="${formName}_SUBMIT=1&${formName}%3A_link_hidden_=&${formName}%3A_idcl=${linkId}&row=${row}&javax.faces.ViewState=$viewstate";
    curl -L -s -b $COOKIE_JAR -c $COOKIE_JAR --data "$payload" "https://ent.normandie-univ.fr$dossierURL/max/action.uP?pP_org.apache.myfaces.portlet.MyFacesGenericPortlet.VIEW_ID=%2Fstylesheets%2Fetu%2Fnotes.xhtml" -o "$HTML_DUMP"
    echo -e "\e[0;32mGrades obtained! \e[0;0m\n"
}

# Asks the user which year
# TODO: review this part and eventually extend for others (Chimie, Elec, ...)
select_grades(){
    tidy -f /dev/null -utf8 "$HTML_DUMP" | tr --delete "\n" > "$HTML_CLEANED"
    # Get folders count
    count=$(xmllint --html --noout "$HTML_CLEANED" --xpath "count(//a[@onclick and contains(text(), 'Informatique')][1]/text())" 2>/dev/null)

    # Display possibilities
    cmd="zenity --list --text 'Select year' --radiolist --column 'Pick' --column 'Year'"
    #ans=$(zenity --list --text "Select year" --radiolist --column "Pick" --column "Year" TRUE "Semestre 1" FALSE "Semestre 2");
    #if command -v zenity >/dev/null 2>&1; then
        ##zenity --list --text 'Select year' --radiolist --column 'Pick' --column 'Year' TRUE "Semestre 1" FALSE "Semestre 2"
        #for ((i=$count; i > 0; i--)); do
            #n=$(($count - $i));
            #cmd="$cmd FALSE '$(xmllint --html --noout "$HTML_CLEANED" --xpath "//a[@onclick and contains(text(), 'Informatique') and contains(@onclick, \"'row','$n'\")]/text()" 2>/dev/null)'"
        #done
        #choice=`eval "$cmd" | colrm 2`
    #else
    if true; then
        echo -e "Années disponibles:"
        for ((i=$count; i > 0; i--)); do
            n=$(($count - $i));
            echo -e "\t$i) $(xmllint --html --noout "$HTML_CLEANED" --xpath "//a[@onclick and contains(text(), 'Informatique') and contains(@onclick, \"'row','$n'\")]/text()" 2>/dev/null)";
        done
        read -p "Quelle année: " choice;
    fi
    # Check input validity
    if [[ "$choice" -le 0 ]] || [[ "$choice" -gt $count ]]; then
        echo -e "\e[0;31mError: please choose a correct value! \e[0;0m";
        exit 1;
    fi

    # Get correct grades folder
    row=$(($count - $choice));
    atag=$(xmllint --html --noout "$HTML_CLEANED" --xpath "//a[@onclick and contains(text(), 'Informatique') and contains(@onclick, \"'row','$row'\")]" 2>/dev/null);
    formName=$(echo "$atag" | cut -d"'" -f2);
    linkId=$(echo "$atag" | cut -d"'" -f4 | sed 's/:/%3A/g');
}

#
# Reformats the obtained (DIRTY!) HTML and narrow it down to the relevant information using XSLT
# /!\ The file might contained abnormalities since the parsing of the original file is a bit clumsy.
# TODO: review this for a better XSLT transformation 
#
format_output(){
    echo "Reformatting HTML..."
    tidy -f /dev/null -utf8 "$HTML_DUMP" > "$HTML_CLEANED";
    sed 's/&nbsp;/@/g' < "$HTML_CLEANED" | tr "\n" " " > "$HTML_DUMP";
    xmllint --html "$HTML_DUMP" --xpath "//tbody" 2>/dev/null | xsltproc --html "$XSL_GRADES" - | tr --delete "@" > "$OUTPUT_GRADES";
    echo -e "\e[0;32mReformatting complete! \e[0;0m"
}

#
# Fetch the coefficents and stores them in an XML file after XSLT transformation
# /!\ The file might contained abnormalities since the parsing of the original file is a bit clumsy.
#
fetch_coefs(){
    echo "Fetching coefficents...";
    if [ ! -f "$OUTPUT_COEFS" ]; then
        curl -s "http://livretpedagogique.ensicaen.fr/pages/afficherSpecialite.php?numSpecialite=1" > "$HTML_DUMP" # numSpecialite=l is for Informatique
        tidy -f /dev/null -utf8 "$HTML_DUMP" > "$HTML_CLEANED"
        xmllint --html --noout "$HTML_CLEANED" --xpath "//div[@id='semestre1']/following-sibling::table/tr[td[a]]" | tr "\n" " " | xsltproc --html "$XSL_COEFS" - > "$OUTPUT_COEFS"
        # Because of the fucking data that were formatted like shit!
        sed -i 's/2EI1/2IE1/g' "$OUTPUT_COEFS";
        echo -e "\e[0;32mCoefficients obtained! \e[0;0m";
    else
        echo -e "\e[0;32m$OUTPUT_COEFS already found!\e[0;0m"
    fi
}

cleanup(){
    rm -f "$SAML_RESPONSE";
    rm -f "$COOKIE_JAR";
    rm -f "$HEADER_DUMP"
    rm -f "$HTML_DUMP"
    rm -f "$HTML_CLEANED"
}


login_cas;
login_shibboleth;
fetch_grades;
format_output;
echo -e "\e[0;33m> Grades in $OUTPUT_GRADES\e[0;0m\n";
fetch_coefs;
echo -e "\e[0;33m> Coefs in $OUTPUT_COEFS\e[0;0m\n";
cleanup;

